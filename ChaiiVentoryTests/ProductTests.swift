//
//  ProductTests.swift
//  ChaiiVentoryTests
//
//  Created by Sohel Seedat on 25/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import XCTest
@testable import ChaiiVentory

class ProductTests: XCTestCase {
	
	var productManager: ProductManager!
	
    override func setUp() {
        super.setUp()
		
		self.productManager = ProductManager()
    }
	
	override func tearDown() {
		super.tearDown()
		
		self.productManager.removeAllProducts()
	}
	
    func testAddProduct() {
		let newProduct = Product(id: 1, name: "Bread", netPrice: 1.20)
		XCTAssertNotNil(newProduct, "Not Nil")
	}
	
	func testAddProductManager() {
		let newProduct = Product(id: 1, name: "Bread", netPrice: 1.20)
		self.productManager.add(product: newProduct)
		XCTAssertTrue(self.productManager.allProducts.count > 0, "There should be at least one item")
	}
	
	func testAddDuplicateProduct() {
		let newProduct = Product(id: 1, name: "Bread", netPrice: 1.20)
		self.productManager.add(product: newProduct)
		
		let newProduct2 = Product(id: 2, name: "Bread", netPrice: 1.20)
		self.productManager.add(product: newProduct2)

		XCTAssertTrue(self.productManager.allProducts.count == 1, "There should be at least one item")
	}
	
	func testAddSimilarProduct() {
		let newProduct = Product(id: 1, name: "Bread", netPrice: 1.20)
		self.productManager.add(product: newProduct)
		
		let newProduct2 = Product(id: 2, name: "Bread Malted", netPrice: 1.20)
		self.productManager.add(product: newProduct2)
		
		XCTAssertTrue(self.productManager.allProducts.count == 2, "There should be at least 2 items")
	}
	
	func testRemoveProduct() {
		let newProduct = Product(id: 1, name: "Bread", netPrice: 1.20)
		self.productManager.add(product: newProduct)
		self.productManager.remove(product: newProduct)
		XCTAssertTrue(self.productManager.allProducts.count == 0, "There should be no items")
	}
	
	func testEditProductPrice() {
		var newProduct = Product(id: 1, name: "Bread", netPrice: 1.20)
		self.productManager.add(product: newProduct)
		newProduct.netPrice = 0.99
		self.productManager.update(product: newProduct)

		let editedProduct = self.productManager.allProducts[0]
		XCTAssertTrue(editedProduct.netPrice == newProduct.netPrice, "The prices should be the same and saved")
	}
	
	func testProductsFromJSON() {
		if let url = Bundle.main.url(forResource: "InventoryList", withExtension: "json") {
			do {
				let data = try Data(contentsOf: url)
				let decoder = JSONDecoder()
				decoder.dateDecodingStrategy = .iso8601
				XCTAssertNoThrow(try decoder.decode(Products.self, from: data), "Should decode json with no errors")
			} catch {
				// throws error
			}
		}
	}
}
