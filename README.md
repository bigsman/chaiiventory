# Inventory List

This app will simplify inventory management. Inspired by Subway and Chaiiwala franchises. 

## To Do

* Complete this ReadMe x_x
* Count Function
- Add weekly count function
* Order Function
* Inventory Function
- Add, Edit and Delete inventory items

## Running the tests

Need to implement unit tests. 

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* Swift - Main language used
* [Firebase](https://github.com/firebase/firebase-ios-sdk) - Firebase is the cloud database

## Authors

* **Sohel Seedat**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

