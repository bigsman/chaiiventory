//
//  Product.swift
//  ChaiiVentory
//
//  Created by Sohel Seedat on 25/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

struct Product: Codable {
	var id: Int
	var name: String
	var netPrice: Double
}

extension Product: Equatable {
	static func == (lhs: Product, rhs: Product) -> Bool {
		return lhs.id == rhs.id && lhs.name == rhs.name
	}
}
