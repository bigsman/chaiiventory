//
//  Products.swift
//  ChaiiVentory
//
//  Created by Sohel Seedat on 30/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

struct Products: Codable {
	let products: [Product]
}
