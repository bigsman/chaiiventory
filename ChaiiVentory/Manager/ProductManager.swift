//
//  ProductManager.swift
//  ChaiiVentory
//
//  Created by Sohel Seedat on 27/10/2017.
//  Copyright © 2017 BigSMan. All rights reserved.
//

import UIKit

class ProductManager {
	
	var allProducts: [Product] = []
	
	func add(product: Product) {
		// Check if objects have same name
		guard self.allProducts.contains(where: { $0.name == product.name }) else {
			self.allProducts.append(product)
			return
		}
	}
	
	func remove(product: Product) {
		if let index = self.allProducts.index(where: { $0 == product }) {
			self.allProducts.remove(at: index)
		}
	}
	
	func update(product: Product) {
		if let index = self.allProducts.index(where: { $0 == product }) {
			self.allProducts.insert(product, at: index)
		}
	}
	
	func removeAllProducts() {
		self.allProducts.removeAll()
	}

}
